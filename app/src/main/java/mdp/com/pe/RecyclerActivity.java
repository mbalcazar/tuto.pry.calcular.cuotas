package mdp.com.pe;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerActivity extends AppCompatActivity {

    ArrayList<String> listDatos;
    RecyclerView recycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        recycler = findViewById(R.id.rviListado);
        recycler.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        listDatos = new ArrayList<String>();

        Intent intent = getIntent();
        String cuota = intent.getStringExtra("cuota");
        String valor = intent.getStringExtra("monto");

        int nrocuotas  = Integer.parseInt(cuota);
        Double monto = Double.parseDouble(valor);
        Double cuota_mes = monto / nrocuotas;

        for(int i=1;i<=nrocuotas;i++){
            listDatos.add("Cuota #"+i+" "+cuota_mes);
        }

        AdapterDatos adapter = new AdapterDatos(listDatos);
        recycler.setAdapter(adapter);
    }
}
