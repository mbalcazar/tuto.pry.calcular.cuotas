package mdp.com.pe;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Spinner spiCuotas;
    Button butCalcular;
    EditText eteMonto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spiCuotas = findViewById(R.id.spiCuotas);
        butCalcular = findViewById(R.id.butCalcular);
        eteMonto = findViewById(R.id.eteMonto);

        llenarSpinnerListado();
        butCalcular.setOnClickListener(this);

    }

    private void llenarSpinnerListado() {
        List<String> lstCuota = new ArrayList<>();

        lstCuota.add("1");
        lstCuota.add("2");
        lstCuota.add("3");
        lstCuota.add("4");
        lstCuota.add("5");
        lstCuota.add("6");
        lstCuota.add("7");
        lstCuota.add("8");
        lstCuota.add("9");
        lstCuota.add("10");
        lstCuota.add("11");
        lstCuota.add("12");

        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, lstCuota);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spiCuotas.setAdapter(dataAdapter);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.butCalcular:
                String valor = eteMonto.getText().toString();
                if (valor.isEmpty() == false) {
                    String Cuota = spiCuotas.getSelectedItem().toString();
                    Intent intent = new Intent(this, RecyclerActivity.class);
                    intent.putExtra("monto", valor);
                    intent.putExtra("cuota", Cuota);
                    startActivityForResult(intent, 1);
                } else {
                    Toast.makeText(this, "Ingresar un valor", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                if(data.hasExtra("retorno")){
                    String mensaje = data.getExtras().getString("retorno");
                    Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
                }

            }
        }
    }
}
